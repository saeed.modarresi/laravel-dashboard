@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(!empty($message))
                    <div class="alert alert-info">
                        @if(is_array($message)){{ data_get($message,0) }}@else{{ $message }}@endif
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
