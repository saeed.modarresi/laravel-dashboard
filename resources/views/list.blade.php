@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(!empty($message))
                    <div class="alert alert-info">
                        @if(is_array($message)){{ data_get($message,0) }}@else{{ $message }}@endif
                    </div>
                @endif
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5>Posts</h5>
                        <a href="{{ route('create') }}" class="btn btn-sm btn-primary">Create Post</a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">Post Title</th>
                        <th class="text-center">Post Author</th>
                        <th class="text-center">Post Description</th>
                        <th class="text-center">Operations</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($data))
                        @foreach ($data as $item)
                            <tr data="{{ $item->id }}">
                                <td class="text-center">{{ $item->post_title }}</td>
                                <td class="text-center">{{ $item->post_author }}</td>
                                <td class="text-center">{{ $item->post_description }}</td>
                                <td class="text-center">
                                    <a href="{{ route('update', $item->id) }}" class="btn btn-sm text-dark p-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </a>
                                    <a href="{{ route('remove', $item->id) }}" class="btn btn-sm text-danger p-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                                        </svg>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
