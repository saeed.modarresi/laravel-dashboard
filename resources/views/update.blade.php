@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5>Update Post</h5>
                        <a href="{{ route('list') }}" class="btn btn-sm btn-primary">Dashboard</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('api.update', !empty($post) ? $post->id : '') }}" method="post" class="form-switch">
                            @csrf
                            <div  class="mb-3">
                                <label for="postTitle">Post Title:</label>
                                <input type="text" name="data[post_title]" id="postTitle" value="@if(!empty($post)){{ $post->post_title }}@endif" class="form-control" required>
                            </div>
                            <div  class="mb-3">
                                <label for="postAuthor">Post Author:</label>
                                <input type="text" name="data[post_author]" id="postAuthor" value="@if(!empty($post)){{ $post->post_author }}@endif" class="form-control">
                            </div>
                            <div  class="mb-3">
                                <label for="postDescription">Post Description:</label>
                                <textarea type="text" name="data[post_description]" id="postDescription" class="form-control" style="height:200px;">@if(!empty($post)){{ $post->post_description }}@endif</textarea>
                            </div>
                            <button type="submit" class="btn btn-dark mt-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
