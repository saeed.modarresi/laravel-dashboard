<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController;
use Closure;
use Illuminate\Http\Request;

class ApiConnectionTest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        /*
        |--------------------------------------------------------------------------
        | API Connection Test
        |--------------------------------------------------------------------------
        |
        | Check the connection to laravel-rest-api is establish or not.
        |
        */
        $api = new ApiController();
        $api = $api->run("GET", "/posts", []);

        if (key_exists('error',$api)) {

            $message = [ 0 => 'Please check the API connect to the laravel-rest-api'.data_get($api,'error') ];
            return response()->view('abort',compact('message'));
        }

        return $next($request);
    }
}
