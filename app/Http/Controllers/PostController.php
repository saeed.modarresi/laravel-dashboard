<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Get posts API
    |--------------------------------------------------------------------------
    |
    | Get all posts data as a list.
    |
    */
    public function posts()
    {

        $api = new ApiController();
        return $api->run("GET", "/", []);
    }


    /*
    |--------------------------------------------------------------------------
    | Show list of posts
    |--------------------------------------------------------------------------
    */
    public function list()
    {

        return view('list')->with(['data' => $this->posts()]);
    }


    /*
    |--------------------------------------------------------------------------
    | Create post API
    |--------------------------------------------------------------------------
    */
    public function create(Request $request)
    {

        if (strtolower($_SERVER['REQUEST_METHOD'] ?? '') == 'get') return view('create');

        $data = $request->all('data');
        $api = new ApiController();

        $res = $api->run("POST", "", $data);

        return view('list')->with([
            'data' => $this->posts(),
            'message' => $res
        ]);
    }


    /*
    |--------------------------------------------------------------------------
    | Update post API
    |--------------------------------------------------------------------------
    */
    public function update(Request $request, $id)
    {

        if (strtolower($_SERVER['REQUEST_METHOD'] ?? '') == 'get'){

            $post = collect($this->posts())->where('id', $id)->first();
            return view('update')->with(['post' => $post]);
        }

        $data = $request->all('data');
        $api = new ApiController();
        $res = $api->run("POST", "/$id", $data);

        return view('list')->with([
            'data' => $this->posts(),
            'message' => $res
        ]);
    }


    /*
    |--------------------------------------------------------------------------
    | Remove post API
    |--------------------------------------------------------------------------
    */
    public function remove($id)
    {
        $api = new ApiController();
        $res = $api->run('DELETE', "/$id", []);

        return view('list')->with([
            'data' => $this->posts(),
            'message' => $res
        ]);
    }
}
