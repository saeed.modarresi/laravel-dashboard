<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class ApiController
{

    public $baseUrl;
    public $apiKey;

    public function __construct()
    {

        $this->baseUrl = env('API_URL');
        $this->apiKey  = env('API_KEY');
    }

    public function run( $method, $uri, $body = [] )
    {

        $uri = $this->baseUrl . $uri;

        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->apiKey,
                'Accept'        => 'application/json',
            ],
            'form_params' => data_get($body,'data', [])
        ];

        try {

            $client = new Client();
            $response = $client->request( $method, $uri, $params);
            $response = $response->getBody()->getContents();
            return json_decode($response);

        } catch ( \Exception $e ) {

            return ['error' => $e->getMessage()];
        }
    }
}
