<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::middleware('apiconnectiontest')->group(function () {

    Route::get('/', 'PostController@list')->name('list');
    Route::get('/create', 'PostController@create')->name('create');
    Route::post('/create', 'PostController@create')->name('api.create');
    Route::get('/update/{id}', 'PostController@update')->name('update');
    Route::post('/update/{id}', 'PostController@update')->name('api.update');
    Route::get('/remove/{id}', 'PostController@remove')->name('remove');

//});
